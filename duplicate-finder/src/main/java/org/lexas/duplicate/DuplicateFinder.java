package org.lexas.duplicate;

import org.lexas.sync.util.DirectoryCreator;
import org.lexas.sync.util.DirectoryUtils;
import org.lexas.sync.util.FileDescriptor;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import picocli.CommandLine;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static org.lexas.sync.util.DirectoryUtils.toPrintDir;

@CommandLine.Command(
        name = "duplicates"
)
public class DuplicateFinder implements Runnable {

    private Path src;

    private Config config;
    private Path backupDir;

    private final DirectoryCreator directoryCreator = new DirectoryCreator();
    private final Map<Path, Result> filesToRemove = new TreeMap<>();

    @Override
    public void run() {

        System.out.println("# scanning " + src.toString());
        final Map<Path, FileDescriptor> allFiles = DirectoryUtils.getFiles(src).entrySet().stream()
                .filter(entry -> !entry.getValue().getBaseName().equals("Thumbs.db") && !entry.getValue().getBaseName().equals(".DS_Store"))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (k1, k2) -> k1));

        final long totalSize = allFiles.values().stream().mapToLong(FileDescriptor::getSize).sum();
        System.out.println("# Found " + allFiles.size() + " files. Total size " + totalSize);

        final Map<HashCodeWrapper, Set<Path>> uniqueFiles = allFiles.entrySet().stream()
                .collect(Collectors.toMap(entry -> {
                    final FileDescriptor fileDescriptor = entry.getValue();
                    return new HashCodeWrapper(fileDescriptor);
                }, entry -> {
                    final Set<Path> result = new TreeSet<>();
                    result.add(entry.getKey());
                    return result;
                }, (l1, l2) -> {
                    l1.addAll(l2);
                    return l1;
                }));
        final long uniqueSize = uniqueFiles.keySet().stream().mapToLong(hd -> hd.fileDescriptor.getSize()).sum();
        System.out.println("# Found " + uniqueFiles.size() + " unique files.  Total size: " + uniqueSize);

        uniqueFiles.entrySet().parallelStream()
                .filter(entry -> entry.getValue().size() > 1)
                .forEach(entry -> {
                    final Set<Path> dups = entry.getValue();
                    processDuplicates(dups, config);
                });


        directoryCreator.collect(DirectoryUtils::printCreateDirCommand);

        filesToRemove.values().forEach(result -> result.print(src, backupDir));
    }

    private void processDuplicates(final Set<Path> dups, final Config config) {
        final Set<Path> toDelete = dups.stream().filter(path -> config.getDeletePatterns().stream()
                .anyMatch(pred -> pred.matcher(path.toString()).matches())
        ).collect(Collectors.toSet());

        if (dups.size() > toDelete.size()) {
            dups.removeAll(toDelete);
        } else {
            toDelete.clear();
        }

        final Set<Path> nonKeep = dups.stream().filter(path -> config.getKeepPatterns().stream()
                .noneMatch(pred -> pred.matcher(path.toString()).matches())
        ).collect(Collectors.toSet());

        if (nonKeep.size() < dups.size()) {
            dups.removeAll(nonKeep);
        } else {
            nonKeep.clear();
        }

        synchronized (this) {
            if (toDelete.size() > 0 || nonKeep.size() > 0) {
                filesToRemove.put(dups.iterator().next(), new Result(dups, toDelete, nonKeep));
                toDelete.forEach(path -> {
                    final Path resolve = backupDir.resolve(src.relativize(path));
                    directoryCreator.addFile(resolve);
                });
                nonKeep.forEach(path -> {
                    final Path resolve = backupDir.resolve(src.relativize(path));
                    directoryCreator.addFile(resolve);
                });
            }
        }
    }

    @CommandLine.Parameters
    public DuplicateFinder setFolders(final String folder) {
        this.src = Paths.get(folder);
        ;
        return this;
    }

    @CommandLine.Option(names = {"--config"})
    public DuplicateFinder setConfig(final File file) {
        try {
            final Yaml yaml = new Yaml(new Constructor(Config.class));
            try (final FileInputStream input = new FileInputStream(file)) {
                config = yaml.load(input);
            }
        } catch (final IOException e) {
            throw new RuntimeException(e);
        } catch (final RuntimeException e) {
            new Object();
        }
        return this;
    }

    @CommandLine.Option(names = {"--backup-dir"}, required = true)
    public DuplicateFinder setBackupDir(final String backupDir) {
        this.backupDir = Paths.get(backupDir);
        return this;
    }

    private static class HashCodeWrapper {
        private final FileDescriptor fileDescriptor;

        public HashCodeWrapper(final FileDescriptor fileDescriptor) {
            this.fileDescriptor = fileDescriptor;
        }

        @Override
        public int hashCode() {
            return fileDescriptor.getBaseName().hashCode();
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof HashCodeWrapper) {
                final HashCodeWrapper hcw = (HashCodeWrapper) obj;
                return hcw.fileDescriptor.getBaseName().equals(fileDescriptor.getBaseName()) &&
                        hcw.fileDescriptor.getSize().longValue() == fileDescriptor.getSize().longValue();
            }
            return false;
        }
    }

    public static class Result {
        Set<Path> dups;
        Set<Path> toDelete;
        Set<Path> nonKeep;

        public Result(final Set<Path> dups, final Set<Path> toDelete, final Set<Path> nonKeep) {
            this.dups = dups;
            this.toDelete = toDelete;
            this.nonKeep = nonKeep;
        }

        public void print(final Path src, final Path backup) {
            dups.forEach(fileName -> System.out.println("# " + fileName));
            if (toDelete.size() > 0) {
                toDelete.forEach(path -> {
                    System.out.println("mv " + toPrintDir(path) + " " + toPrintDir(backup.resolve(src.relativize(path))) + " # toDelete");
                });
            }
            if (nonKeep.size() > 0) {
                nonKeep.forEach(path -> {
                    System.out.println("mv " + toPrintDir(path) + " " + toPrintDir(backup.resolve(src.relativize(path))) + " # nonKeep");
                });
            }
        }
    }
}