package org.lexas.duplicate;

import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Config {
    private List<String> keep = Collections.EMPTY_LIST;
    private List<Pattern> keepPatterns = Collections.EMPTY_LIST;

    private List<String> delete = Collections.EMPTY_LIST;
    private List<Pattern> deletePatterns = Collections.EMPTY_LIST;

    public List<String> getKeep() {
        return keep;
    }

    public void setKeep(final List<String> keep) {
        this.keep = keep;
        this.keepPatterns = keep.stream()
                .map(Pattern::compile)
                .collect(Collectors.toList());
    }

    public List<Pattern> getKeepPatterns() {
        return keepPatterns;
    }


    public List<String> getDelete() {
        return delete;
    }

    public void setDelete(final List<String> delete) {
        this.delete = delete;
        this.deletePatterns = delete.stream()
                .map(Pattern::compile)
                .collect(Collectors.toList());
    }

    public List<Pattern> getDeletePatterns() {
        return deletePatterns;
    }
}
