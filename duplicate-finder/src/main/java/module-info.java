module org.lexas.syncer.duplicates {
    requires org.lexas.syncer.dir;
    requires org.yaml.snakeyaml;
    requires info.picocli;
    exports org.lexas.duplicate;
}