package org.lexas.synccli;

import org.lexas.duplicate.DuplicateFinder;
import org.lexas.sync.Syncer;
import picocli.AutoComplete;
import picocli.CommandLine;

import java.io.IOException;

@CommandLine.Command(
        subcommands = {
                DuplicateFinder.class,
                Syncer.class,
                AutoComplete.GenerateCompletion.class,
                CommandLine.HelpCommand.class
        }
)
public class Main implements Runnable {
    public static void main(final String[] args) throws IOException {
        new CommandLine(new Main()).execute(args);
    }

    @Override
    public void run() {

    }
}
