module org.lexas.syncer.main {
    requires org.lexas.syncer.dir;
    requires org.lexas.syncer.duplicates;
    requires org.lexas.syncer.impl;
    requires info.picocli;
}