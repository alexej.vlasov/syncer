#!/usr/bin/env sh

# docker build -t registry.gitlab.com/alexej.vlasov/syncer-docker/syncer:latest .

MASTERDIR=/media/master/storage/gallery/

docker pull registry.gitlab.com/alexej.vlasov/syncer/syncer:latest

docker run -v /media:/media:ro -v /root/proj/syncer/photo.yaml:/config/config.yaml:ro registry.gitlab.com/alexej.vlasov/syncer/syncer:latest duplicates --config=/config/config.yaml --backup-dir=/media/master/backup/gallery/ $MASTERDIR


#  rsync -a --dry-run --delete-delay  /master/ /slave/
