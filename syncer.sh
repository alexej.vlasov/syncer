#!/usr/bin/env sh

#docker build -t registry.gitlab.com/alexej.vlasov/syncer-docker/syncer:latest .

MASTERDIR=/media/master/storage
SLAVEDIR=/media/slave/storage

DATE=`date '+%Y-%m-%d_%H-%M-%S'`

docker pull registry.gitlab.com/alexej.vlasov/syncer/syncer:latest

docker run -v /:/.docker_syncer_root:ro  registry.gitlab.com/alexej.vlasov/syncer/syncer:latest detect-rename /.docker_syncer_root/$MASTERDIR /.docker_syncer_root/$SLAVEDIR
