FROM adoptopenjdk/openjdk11:x86_64-alpine-jdk-11.0.5_10-slim
ADD main/target/main-1.0-SNAPSHOT-jar-with-dependencies.jar /main.jar
ENTRYPOINT ["java", "-jar", "/main.jar"]
