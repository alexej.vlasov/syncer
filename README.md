# syncer

This tool is designed to deal with directory structure changes (moving files/renaming files or directories) before rsync run. 

Rsync is awesome tool to sync files and directories but it doesn't support directory renaming. If some of directory changed its name rsync copies files under new directory name and delete or move to backup dir if --backup option is used. It causes a lot of excessive IO acitvities. There is detect-renamed path for rsync but it seems like it doesn't work very well. I was not able to make it run. 

The tools prevents this behavior. It compares files in directories (disregarding the directory name. just file name/length/and modification datetime). if directory renaming is detecting it create a scipt with list of the files to move.

 