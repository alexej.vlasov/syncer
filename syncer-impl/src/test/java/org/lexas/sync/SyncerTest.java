package org.lexas.sync;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.lexas.sync.util.DirectoryUtils;
import org.lexas.sync.util.FileDescriptor;
import org.lexas.sync.util.FileDescriptorKey;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.BiConsumer;

public class SyncerTest {


    private Syncer syncer;
    private static String rootDir;

    @BeforeAll
    public static void setUpClass() throws URISyntaxException {
        URI uri = SyncerTest.class.getClassLoader().getResource("org/lexas/sync/SyncerTest.class").toURI();
        Path path = Paths.get(uri);
        Path root = path.subpath(0, path.getNameCount() - 6);
        rootDir = "/" + root.toString();
    }

    @BeforeEach
    public void setUp() {
        syncer = new Syncer();
    }

    @Test
    public void test() throws IOException, URISyntaxException {
        final String[] args = new String[]{rootDir + "/src/test/file/src", rootDir + "/src/test/file/dst"};
        final BiConsumer<FileDescriptor, Path> SOUT_ACTION = (srcDescriptor, dstPath) -> {
        };
        syncer.start(Paths.get(args[0]), Paths.get(args[1]), SOUT_ACTION, FileDescriptorKey.NAME);
    }

    public void test1() throws IOException, URISyntaxException {
        String drive = "/Volumes/SLAVE-DATA";
        //String srcRoot = drive + "/backup/2019-04-28_21-20-12/";
        String srcRoot = drive + "/backup/2019-07-20_07-58-17/";
        //String srcRoot = drive + "/backup/2019-07-29_00-50-26/";
        String dstRoot = drive + "/media/";
        final String[] args = new String[]{srcRoot, dstRoot};

        final BiConsumer<FileDescriptor, Path> SOUT_ACTION = (srcDescriptor, dstPath) -> {
        };
        syncer.start(Paths.get(args[0]), Paths.get(args[1]), SOUT_ACTION, FileDescriptorKey.NAME);
    }

    @Test
    public void testCreateDirCommand() {
        Syncer syncer = new Syncer();
        DirectoryUtils.printCreateDirCommand(Path.of("master", "gallery", "ФОТО 2018 год"));
    }

}