module org.lexas.syncer.impl {
    requires org.lexas.syncer.dir;
    requires org.apache.commons.collections4;
    requires com.github.spotbugs.annotations;
    requires info.picocli;
    exports org.lexas.sync;
}