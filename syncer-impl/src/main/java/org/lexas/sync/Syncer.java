package org.lexas.sync;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.lexas.sync.util.DirectoryCreator;
import org.lexas.sync.util.DirectoryUtils;
import org.lexas.sync.util.FileDescriptor;
import org.lexas.sync.util.FileDescriptorKey;
import picocli.CommandLine;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.BiConsumer;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.lexas.sync.util.DirectoryUtils.getRelativizedFiles;

@CommandLine.Command(
        name = "detect-rename"
)
public class Syncer implements Runnable {

    public static final String ROOT = "/";
    public static final String DOCKER_SYNCER_ROOT = "/.docker_syncer_root";

    @CommandLine.Parameters(index = "0")
    private String source;

    @CommandLine.Parameters(index = "1")
    private String destination;

    public void run() {
        final Path srcRoot = Paths.get(source);
        final Path dstRoot = Paths.get(destination);

        final Map<Path, Path> filesToMove = new TreeMap<>();
        final DirectoryCreator directoryCreator = new DirectoryCreator();
        final BiConsumer<FileDescriptor, Path> ON_MOVE_ACTION = (srcDescriptor, dstRelativePath) -> {
            final Path dstPath = dstRoot.resolve(dstRelativePath);
            directoryCreator.addFile(dstPath);
            filesToMove.put(srcDescriptor.getAbsolutePath(), dstPath);
        };
        start(srcRoot, dstRoot, ON_MOVE_ACTION, FileDescriptorKey.NAME);

        final Path realRoot = getRealRoot(srcRoot, dstRoot);
        directoryCreator.collect(dir -> DirectoryUtils.printCreateDirCommand(realRoot.relativize(dir)));
        filesToMove.forEach((src, dst) -> System.out.println("mv /" + DirectoryUtils.toPrintDir(realRoot.relativize(src)) + " /" + DirectoryUtils.toPrintDir(realRoot.relativize(dst))));
    }

    @SuppressFBWarnings("DMI_HARDCODED_ABSOLUTE_FILENAME")
    private static Path getRealRoot(final Path srcRoot, final Path dstRoot) {
        final Path realRoot;
        if (srcRoot.startsWith(DOCKER_SYNCER_ROOT) && dstRoot.startsWith(DOCKER_SYNCER_ROOT)) {
            realRoot = Paths.get(DOCKER_SYNCER_ROOT);
        } else {
            realRoot = Paths.get(ROOT);
        }
        return realRoot;
    }


    public void start(final Path src, final Path dst, final BiConsumer<FileDescriptor, Path> onMoveAction, final EnumSet<FileDescriptorKey.ComparingOption> comparingOptions) {
        // build files list for source directory
        final Map<String, FileDescriptor> srcFiles = getRelativizedFiles(src);
        // build files list for destination directory
        final Map<String, FileDescriptor> dstFiles = getRelativizedFiles(dst);

        // build file list for files from destination directory that doesn't have matched source
        final Map<String, FileDescriptor> candidatesToDelete = new HashMap<>(dstFiles);
        candidatesToDelete.keySet().removeAll(srcFiles.keySet());

        // And build the index to perform matches
        final MultiValuedMap<FileDescriptorKey, FileDescriptor> index = buildIndex(candidatesToDelete, comparingOptions);

        // remove all files from the source that are already in dest.
        // If they are changed - rsync will sync them, otherwise there is nothing to do with them.
        srcFiles.keySet().removeAll(dstFiles.keySet());

        // for each file from the rest let's check if there is match from the destination.
        srcFiles.forEach((k, v) -> {
            final Collection<FileDescriptor> fileDescriptors = index.get(new FileDescriptorKey(v, comparingOptions));
            if (!isEmpty(fileDescriptors)) {
                final FileDescriptor targetMatch = fileDescriptors.iterator().next();
                fileDescriptors.remove(targetMatch);
                dst.resolve(v.getRelativePath());
                onMoveAction.accept(targetMatch, v.getRelativePath());
            }
        });
    }

    private MultiValuedMap<FileDescriptorKey, FileDescriptor> buildIndex(final Map<String, FileDescriptor> dstFiles2, final EnumSet<FileDescriptorKey.ComparingOption> compareOptions) {
        final MultiValuedMap<FileDescriptorKey, FileDescriptor> destFiles = new ArrayListValuedHashMap<>();
        dstFiles2.values().forEach(v -> destFiles.put(new FileDescriptorKey(v, compareOptions), v));
        return destFiles;
    }
}
