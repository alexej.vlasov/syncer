#!/usr/bin/env sh

MASTERDIR=/media/master/storage
SLAVEDIR=/media/slave/storage

BACKUPDIR=/media/slave/backup

DATE=`date '+%Y-%m-%d_%H-%M-%S'`

rsync -a --delete-delay --backup --backup-dir=$BACKUPDIR/$DATE $MASTERDIR/ $SLAVEDIR/