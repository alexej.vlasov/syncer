package org.lexas.sync.util;

import java.nio.file.Path;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Consumer;

public class DirectoryCreator {

    private final Set<Path> dirsToCreate = new TreeSet<>();

    public void addFile(final Path path) {
        createDirs(path.getParent());
    }

    private void createDirs(final Path dir) {
        if (dir == null) {
            return;
        }
        if (!dirsToCreate.contains(dir) && !dir.toFile().exists()) {
            createDirs(dir.getParent());
            dirsToCreate.add(dir);
        }
    }

    public void collect(final Consumer<Path> dirCreator) {
        dirsToCreate.forEach(dirCreator);
    }
}
