package org.lexas.sync.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

public final class DirectoryUtils {
    public static Map<String, FileDescriptor> getRelativizedFiles(final Path start) {
        return getFilesStream(start)
                .collect(toMap(
                        fd -> fd.getRelativePath().toString(),
                        Function.identity(),
                        (a1, a2) -> a1));
    }

    public static Map<Path, FileDescriptor> getFiles(final Path start) {
        return getFilesStream(start)
                .collect(toMap(
                        FileDescriptor::getAbsolutePath,
                        Function.identity(), (a1, a2) -> a1));
    }

    private static Stream<FileDescriptor> getFilesStream(final Path start) {
        try {
            return Files.walk(start)
                    .map(path -> {
                        try {
                            final BasicFileAttributes basicFileAttributes = Files.readAttributes(path, BasicFileAttributes.class);
                            if (basicFileAttributes.isDirectory()) {
                                return null;
                            }
                            final Path absolutePath = path.toAbsolutePath();
                            return new FileDescriptor(absolutePath, start.relativize(absolutePath), path.getFileName().toString(), basicFileAttributes);
                        } catch (final IOException ioe) {
                            return null;
                        }
                    })
                    .filter(Objects::nonNull);

        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void printCreateDirCommand(final Path path) {
        System.out.println("mkdir " + toPrintDir(path));

    }

    public static String toPrintDir(final Path s1) {
        return s1.toString().replaceAll(" ", "\\\\ ").replaceAll("\\(", "\\\\(").replaceAll("\\)", "\\\\)");
    }
}
