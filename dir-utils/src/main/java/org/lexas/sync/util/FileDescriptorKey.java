package org.lexas.sync.util;

import java.util.EnumSet;

public class FileDescriptorKey {

    private final FileDescriptor fileDescriptor;

    private final EnumSet<ComparingOption> compareOptions;

    public enum ComparingOption {
        BY_NAME,
        BY_SHORT_HASH,
        BY_HASH
    }

    public final static EnumSet<ComparingOption> NAME = EnumSet.of(ComparingOption.BY_NAME);

    public FileDescriptorKey(final FileDescriptor fileDescriptor, EnumSet<ComparingOption> compareOptions) {
        this.fileDescriptor = fileDescriptor;
        this.compareOptions = compareOptions;
    }

    @Override
    public int hashCode() {
        return Long.hashCode(fileDescriptor.getSize());
    }


    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof FileDescriptorKey) {
            final FileDescriptorKey dst = (FileDescriptorKey) obj;

            if (!dst.fileDescriptor.getSize().equals(fileDescriptor.getSize())) {
                return false;
            }

            if (compareOptions.contains(ComparingOption.BY_NAME) && !dst.fileDescriptor.getBaseName().equals(fileDescriptor.getBaseName())) {
                return false;
            }

            if (compareOptions.contains(ComparingOption.BY_SHORT_HASH) && !dst.fileDescriptor.getShortHash().equals(fileDescriptor.getShortHash())) {
                return false;
            }

            if (compareOptions.contains(ComparingOption.BY_HASH) && dst.fileDescriptor.getFullHash().equals(fileDescriptor.getFullHash())) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }
}
