package org.lexas.sync.util;

import org.apache.commons.io.input.BoundedInputStream;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.NoSuchAlgorithmException;

public class FileDescriptor {

    private final Path absolutePath;

    private final Path relativePath;

    private final String baseName;

    private final BasicFileAttributes fileAttributes;

    private Long timestamp;

    private String shortHash;

    private String fullHash;

    public FileDescriptor(final Path absolutePath, final Path relativePath, final String baseName, final BasicFileAttributes basicFileAttributes) {
        this.absolutePath = absolutePath;
        this.relativePath = relativePath;
        this.baseName = baseName;
        this.fileAttributes = basicFileAttributes;
    }



    public String getShortHash() {
        if (shortHash != null) {
            return shortHash;
        } else {
            try (final FileInputStream stream = new FileInputStream(absolutePath.toFile())) {
                final InputStream is = new BoundedInputStream(new BufferedInputStream(stream), 512);
                shortHash = HashUtil.getSha256HASH(is);
                return shortHash;
            } catch (final IOException | NoSuchAlgorithmException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public String getFullHash() {
        if (fullHash != null) {
            return fullHash;
        } else {
            try (final FileInputStream stream = new FileInputStream(absolutePath.toFile());) {
                fullHash = HashUtil.getSha256HASH(new BufferedInputStream(stream, 8192));
                return fullHash;
            } catch (final IOException | NoSuchAlgorithmException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public Long getSize() {
        return fileAttributes.size();
    }

    public Long getModified() {
        if (timestamp == null) {
            timestamp = fileAttributes.lastModifiedTime().toMillis();
        }
        return timestamp;
    }

    public String getBaseName() {
        return baseName;
    }

    public Path getRelativePath() {
        return relativePath;
    }

    public Path getAbsolutePath() {
        return absolutePath;
    }
}
