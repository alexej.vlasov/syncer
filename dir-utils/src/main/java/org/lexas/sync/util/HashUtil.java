package org.lexas.sync.util;

import java.io.IOException;
import java.io.InputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashUtil {
    public static String getSha256HASH(final InputStream is) throws NoSuchAlgorithmException, IOException {
        MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
        return getStreamHash(sha256, is);
    }


    public static String getStreamHash(MessageDigest md, final InputStream stream) throws IOException {
        // file hashing with DigestInputStream
        //SHA, MD2, MD5, SHA-256, SHA-384...
        try (DigestInputStream dis = new DigestInputStream(stream, md)) {
            while (dis.read() != -1) {
                //empty loop to clear the data
            }
            md = dis.getMessageDigest();
        }

        // bytes to hex
        StringBuilder result = new StringBuilder();
        for (byte b : md.digest()) {
            result.append(String.format("%02x", b));
        }
        return result.toString();
    }
}
